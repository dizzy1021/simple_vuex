import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict:true,
    state: {
        products:[
            {name:'Banana Skin', price:20},
            {name:'Shiny Star', price:30},
            {name:'Apple Malang', price:10},
            {name:'Coconut Boys', price:50},
            {name:'Melon Doto', price:30},
        ]
    },
    getters: {
        saleProducts: state => {
            var saleProducts = state.products.map( product => {
                return {
                  name:'**'+product.name+"**",
                  price:product.price/2
                }
            } );
            return saleProducts;
        }
    },
    mutations:{
        reducePrice: (state,payload) => {
            state.products.forEach( product=>{
                product.price -= payload;
            });
        }
    },
    actions:{
        reducePrice: (context,payload) => {
            setTimeout(function(){
                context.commit('reducePrice',payload);
            },2000);
        }
    }
});